extends Control

var scene_path_to_load

func _ready():
	$Menu/VBoxContainer/CenterContainer/Buttons/PlayGameButton.grab_focus()
	for button in $Menu/VBoxContainer/CenterContainer/Buttons.get_children():
		button.connect("pressed",self,"_on_Button_Pressed", [button.scene_to_load])
	pass

func _on_Button_Pressed(scene_to_load):
	scene_path_to_load = scene_to_load

	$FadeIn.show()
	$FadeIn.fade_in()
	
	pass

func _on_FadeIn_fade_finished():
	get_tree().change_scene(scene_path_to_load)
	pass # replace with function body
